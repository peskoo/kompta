package fr.cnam.kompta.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import fr.cnam.kompta.R;
import fr.cnam.kompta.activities.MainActivity;
import fr.cnam.kompta.classes.DatabaseHelper;
import fr.cnam.kompta.classes.Element;
import fr.cnam.kompta.classes.Recurrence;

public class ElementAdapter extends BaseAdapter {
    private final String TAG = "ElementAdapter";
    private Context context;
    private ArrayList<Element> elements;
    private LayoutInflater inflater;
    public ElementAdapter(Context context, ArrayList<Element> elements) {
        this.context = context;
        this.elements = elements;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return elements.size();
    }

    @Override
    public Element getItem(int i) {
        return elements.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.getItem(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.adapter_element, null);

        TextView name = view.findViewById(R.id.adapter_element_name_txt);
        name.setText(this.getItem(i).getName());

        TextView price = view.findViewById(R.id.adapter_element_price_txt);
        price.setText(String.valueOf(this.getItem(i).getPrice()));

        TextView recurrenceTextView = view.findViewById(R.id.adapter_element_recurrence_txt);
        setRecurrenceAcronymToView(recurrenceTextView, this.getItem(i).getRecurrence());

        ImageButton deleteBtn = view.findViewById(R.id.adapter_element_delete_btn);
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog(String.valueOf(getItemId(i)));
            }
        });

        return view;
    }

    public void setRecurrenceAcronymToView(TextView recurrenceTextView, Recurrence recurrence) {
        if (recurrence == Recurrence.DAILY) {
            recurrenceTextView.setText("D");
        }
        if (recurrence == Recurrence.MONTHLY) {
            recurrenceTextView.setText("M");
        }
        if (recurrence == Recurrence.YEARLY) {
            recurrenceTextView.setText("Y");
        }
    }
    public void openDialog(String itemId){
        // Build Alert Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Confirm");
        builder.setMessage("Are you sure?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                DatabaseHelper db = new DatabaseHelper(context);
                db.deleteElement(itemId);

                // Refresh activity from here.
                ((MainActivity) context).recreate();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
