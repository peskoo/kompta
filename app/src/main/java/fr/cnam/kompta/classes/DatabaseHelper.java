package fr.cnam.kompta.classes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {
    private Context context;
    private static final String TAG = "DatabaseHelper";
    private static final String DATABASE_NAME = "kompta.db";
    private static final int DATABASE_VERSION = 1;

    private static final String ELEMENT_TABLE_NAME = "elements";
    private static final String ELEMENT_COLUMN_ID = "_id";
    private static final String ELEMENT_COLUMN_CATEGORY = "category";
    private static final String ELEMENT_COLUMN_NAME = "name";
    private static final String ELEMENT_COLUMN_PRICE = "price";
    private static final String ELEMENT_COLUMN_RECURRENCE = "recurrence";

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String element_query_table =
                "CREATE TABLE " + ELEMENT_TABLE_NAME + " (" +
                        ELEMENT_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        ELEMENT_COLUMN_CATEGORY + " TEXT NOT NULL, " +
                        ELEMENT_COLUMN_NAME + " TEXT NOT NULL, " +
                        ELEMENT_COLUMN_PRICE + " REAL NOT NULL, " +
                        ELEMENT_COLUMN_RECURRENCE + " TEXT NOT NULL );";

        db.execSQL(element_query_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e(TAG, "Updating table from " + oldVersion + " to " + newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + ELEMENT_TABLE_NAME + ";");
        onCreate(db);
    }

    public void addElement(String category, String name, double price, Recurrence recurrence) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Les données de l'app seront passées à la base de donnée via ContentValues.
        ContentValues contentValues = new ContentValues();

        // key: values --> key: column_name ; values: data
        contentValues.put(ELEMENT_COLUMN_CATEGORY, category);
        contentValues.put(ELEMENT_COLUMN_NAME, name);
        contentValues.put(ELEMENT_COLUMN_PRICE, price);
        contentValues.put(ELEMENT_COLUMN_RECURRENCE, recurrence.name());

        long result = db.insert(ELEMENT_TABLE_NAME, null, contentValues);
        if (result == -1){
            Toast.makeText(context, "Error during adding new element.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Element added with success !", Toast.LENGTH_SHORT).show();
        }
    }

    public Cursor listElements() {
        String query = "SELECT * FROM " + ELEMENT_TABLE_NAME + " ORDER BY " + ELEMENT_COLUMN_NAME + " ASC;";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        if (db != null){
            cursor = db.rawQuery(query, null);
        }
        return cursor;
    }

    public void deleteElement(String elementId){
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.delete(ELEMENT_TABLE_NAME, "_id=?", new String[]{elementId});
        if (result == -1){
            Toast.makeText(context, "Impossible to delete this element.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Element deleted with success !", Toast.LENGTH_SHORT).show();
        }
    }



}
