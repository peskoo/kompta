package fr.cnam.kompta.classes;

public class Element {
    private final static String TAG = "ElementClass";
    private String name, category;
    private Integer id;
    private double price;
    private Recurrence recurrence;

    public Element(String category, Integer id, String name, double price, Recurrence recurrence){
        this.category = category;
        this.id = id;
        this.name = name;
        this.price = price;
        this.recurrence = recurrence;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public void setRecurrence(Recurrence recurrence) {
        this.recurrence = recurrence;
    }
    public String getCategory() {
        return category;
    }
    public Integer getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public double getPrice() {
        return price;
    }
    public Recurrence getRecurrence() {
        return recurrence;
    }
}
