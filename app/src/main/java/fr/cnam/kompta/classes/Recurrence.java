package fr.cnam.kompta.classes;

public enum Recurrence {
    DAILY,
    MONTHLY,
    YEARLY
}