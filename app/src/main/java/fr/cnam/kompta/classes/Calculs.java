package fr.cnam.kompta.classes;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;

public class Calculs {
    private final static String TAG = "CalculsClass";
    private ArrayList<Element> elements;
    public Calculs(ArrayList<Element> elements){
        this.elements = elements;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public Double getAverageByMonth() {
        Double result = 0.0;
        for (Element element : this.elements){
            if (element.getRecurrence() == Recurrence.DAILY) {
                Calendar c = Calendar.getInstance();
                int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
                result += element.getPrice() * monthMaxDays;
            }

            if (element.getRecurrence() == Recurrence.MONTHLY) {
                result += element.getPrice();
            }

            if (element.getRecurrence() == Recurrence.YEARLY) {
                result += element.getPrice() / 12;
            }
        }
        return round(result, 2);
    }

    public Double getAverageByYear() {
        Double result = 0.0;
        result += this.getAverageByMonth() * 12;
        return round(result, 2);
    }
}
