package fr.cnam.kompta.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import java.util.ArrayList;

import fr.cnam.kompta.R;
import fr.cnam.kompta.classes.DatabaseHelper;
import fr.cnam.kompta.classes.Element;
import fr.cnam.kompta.classes.Recurrence;

public class addElementActivity extends AppCompatActivity {
    private final static String TAG = "addElementActivity";
    private DatabaseHelper db;
    private Recurrence recurrence = Recurrence.MONTHLY;
    private EditText nameEditText, priceEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_element);

        nameEditText = findViewById(R.id.txt_name);
        priceEditText = findViewById(R.id.txt_price);
        Button okBtn = findViewById(R.id.btn_ok);
        Button cancelBtn = findViewById(R.id.btn_cancel);

        // Database
        db = new DatabaseHelper(getApplicationContext());

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Retrieve informations from user
                // Then add new Element in database.
                String name = nameEditText.getText().toString();
                double price = Double.parseDouble(priceEditText.getText().toString());
                db.addElement("default", name, price, recurrence);

                Log.d(TAG, "new Element obj added to database: " + name + " - " + price + " - " + recurrence);
                finish();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_btn_daily:
                if (checked)
                    this.recurrence = Recurrence.DAILY;
                    Log.d(TAG,"Recurrence checked: " + recurrence);
                    break;
            case R.id.radio_btn_monthly:
                if (checked)
                    this.recurrence = Recurrence.MONTHLY;
                    Log.d(TAG, "Recurrence checked: " + recurrence);
                break;
            case R.id.radio_btn_yearly:
                if (checked)
                    this.recurrence = Recurrence.YEARLY;
                    Log.d(TAG, "Recurrence checked: " + recurrence);
                break;
        }
    }

}