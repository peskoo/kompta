package fr.cnam.kompta.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import fr.cnam.kompta.R;
import fr.cnam.kompta.adapters.ElementAdapter;
import fr.cnam.kompta.classes.Calculs;
import fr.cnam.kompta.classes.DatabaseHelper;
import fr.cnam.kompta.classes.Element;
import fr.cnam.kompta.classes.Recurrence;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "MainActivity";
    private DatabaseHelper db;
    private ArrayList<String> element_category, element_name, element_recurrence;
    private ArrayList<Integer> element_id;
    private ArrayList<Double> element_price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ------------------------------- //
        // ---------- List view ---------- //
        // ------------------------------- //
        ArrayList<Element> allElementsObj = retrieveElementsFromDatabase();

        ListView elementsListView = findViewById(R.id.listview_elements);
        ElementAdapter elementAdapter = new ElementAdapter(this, allElementsObj);
        elementsListView.setAdapter(elementAdapter);

        // ------------------------------- //
        // --------- New Element --------- //
        // ------------------------------- //
        FloatingActionButton addElementBtn = findViewById(R.id.add_element_btn);
        addElementBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addElementView = new Intent(MainActivity.this, addElementActivity.class);
                startActivity(addElementView);
            }
        });

        // ------------------------------- //
        // ---------- Calculs ------------ //
        // ------------------------------- //
        TextView monthlyTextView = findViewById(R.id.total_monthly_text);
        TextView yearlyTextView = findViewById(R.id.total_yearly_text);

        Calculs calculs = new Calculs(allElementsObj);
        monthlyTextView.setText("/monthly: " + String.valueOf(calculs.getAverageByMonth()) + " €");
        yearlyTextView.setText("/yearly: " + String.valueOf(calculs.getAverageByYear()) + " €");

    }
    @Override
    protected void onResume() {
        super.onResume();
        ArrayList<Element> allElementsObj = retrieveElementsFromDatabase();

        ListView elementsListView = findViewById(R.id.listview_elements);
        ElementAdapter elementAdapter = new ElementAdapter(this, allElementsObj);
        elementsListView.setAdapter(elementAdapter);

        // ------------------------------- //
        // ---------- Calculs ------------ //
        // ------------------------------- //
        TextView monthlyTextView = findViewById(R.id.total_monthly_text);
        TextView yearlyTextView = findViewById(R.id.total_yearly_text);

        Calculs calculs = new Calculs(allElementsObj);
        monthlyTextView.setText("/monthly: " + String.valueOf(calculs.getAverageByMonth()) + " €");
        yearlyTextView.setText("/yearly: " + String.valueOf(calculs.getAverageByYear()) + " €");
    }

    private ArrayList<Element> fromDatabaseToArrayOfElementsObject(ArrayList<Integer> ids, ArrayList<String> categories, ArrayList<String> names, ArrayList<Double> prices, ArrayList<String> recurrences) {
        ArrayList<Element> elements = new ArrayList<>();
        if (ids.size() >= 1) {
            for (int i = 0; i < ids.size(); i++) {
                Element el = new Element(categories.get(i), ids.get(i), names.get(i), prices.get(i), Recurrence.valueOf(recurrences.get(i)));
                elements.add(el);
            }
        }
        return elements;
    }

    private ArrayList<Element> retrieveElementsFromDatabase() {
        db = new DatabaseHelper(getApplicationContext());

        Cursor cursor = db.listElements();
        this.element_id = new ArrayList<>();
        this.element_category = new ArrayList<>();
        this.element_name = new ArrayList<>();
        this.element_price = new ArrayList<>();
        this.element_recurrence = new ArrayList<>();

        if (cursor.getCount() == 0){
            Toast.makeText(this, "There is no element yet.", Toast.LENGTH_SHORT).show();
        } else {
            while (cursor.moveToNext()) {
                this.element_id.add(cursor.getInt(0));
                this.element_category.add(cursor.getString(1));
                this.element_name.add(cursor.getString(2));
                this.element_price.add(cursor.getDouble(3));
                this.element_recurrence.add(cursor.getString(4));
            }
        }

        ArrayList<Element> elements = fromDatabaseToArrayOfElementsObject(
                this.element_id, this.element_category, this.element_name, this.element_price, this.element_recurrence
        );
        Log.d(TAG, "elements: " + elements);

        return elements;
    }
}